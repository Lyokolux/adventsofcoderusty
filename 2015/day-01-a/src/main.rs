const INPUT: &'static str = include_str!("../input-a.txt");

fn main() {
    let mut level: i16 = 0;

    for c in INPUT.chars() {
        match c {
            '(' => level += 1,
            ')' => level -= 1,
            _   => (),
        }
    }
    println!("{}", level);
}
